#!/usr/bin/env python

import os
import sys
import pandas as pd

target_dir = sys.argv[1]

print('Loading llikelihood matrix...')
llikelihood_df = pd.read_csv(os.path.join(target_dir, 'llikelihood.matrix'), header=0, index_col=0, delimiter='\t')

print('Calculating averages and standard deviations...')
means = llikelihood_df.mean(axis=1)
stdev = llikelihood_df.std(axis=1)

print('Creating null parameters...')
nullparams_df = pd.concat([means, stdev], axis=1)

print('Saving null parameters...')
nullparams_df.to_csv(os.path.join(target_dir, 'nullParameters.tsv'), sep='\t', header=False)
