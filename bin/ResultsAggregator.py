#!/usr/bin/env python
import sys
from pathlib import Path
import pandas as pd
from pandas.errors import ParserError
import numpy as np

pd.set_option('display.max_rows', 100)
pd.set_option('display.max_columns', 10)
pd.set_option('display.width', 1000)

project_dir = Path(sys.argv[1])

crispr_fp = project_dir / 'CRISPR_Host_spacers_vs_Viruses.bln'
crispr_header = ['qseqid', 'sseqid', 'pident', 'length', 'qlen', 'slen', 'qstart', 'qend', 'sstart', 'send',
                 'evalue', 'bitscore']

if crispr_fp.exists():

    crispr_df = pd.read_csv(crispr_fp, header=None, names=crispr_header, index_col=None, delimiter='\t')
    crispr_df['bitscore'] = crispr_df['bitscore'].str.replace(',', '')

    crispr_df['mismatch'] = crispr_df['qlen'] - crispr_df['length']

    crispr_df = crispr_df[(crispr_df['pident'] == 100) & (crispr_df['mismatch'] <= 1)]

    crispr_df = crispr_df.sort_values(by='mismatch', ascending=True)

    crispr_df = crispr_df.drop_duplicates(subset=['qseqid', 'sseqid'], keep='first')

    # Is this all 26 is doing?
    crispr_df['qseqid'] = crispr_df['qseqid'].apply(lambda x: x.split('_Contig_')[0])

    # crispr2_df = crispr_df[['qseqid', 'sseqid', 'mismatch']]
    # crispr2_df['mismatch'] = crispr_df[crispr_df['mismatch'] == 1]

    # Lines 28-43
    crispr_matches_df = crispr_df.groupby(['qseqid', 'sseqid']).size().reset_index().rename(
        columns={0: 'Number_of_CRISPR_matches'})
    crispr_max_mm_df = crispr_df.groupby(['qseqid', 'sseqid'])['mismatch'].max().reset_index().rename(
        columns={'mismatch': 'Max_number_of_mismatches'})

    crispr_merged_df = pd.merge(crispr_matches_df, crispr_max_mm_df, how='left', on=['qseqid', 'sseqid'])

    crispr_merged_df = crispr_merged_df.rename(columns={'qseqid': 'Host', 'sseqid': 'Virus'})

    # Scoring
    crispr_merged_df['CRISPR_score'] = 0
    for i, df in crispr_merged_df.iterrows():
        score = 0
        if df['Number_of_CRISPR_matches'] >= 1:
            score += df['Number_of_CRISPR_matches'] + 2  # 1st perfect match = 3, then +1 for each additional
        if df['Max_number_of_mismatches'] == 1:
            score -= 1

        crispr_merged_df.loc[i, 'CRISPR_score'] = score

    # Will need to do this eventually
    crispr_merged_df.index = pd.MultiIndex.from_arrays(crispr_merged_df[['Host', 'Virus']].values.T)

else:
    crispr_merged_df = pd.DataFrame()

# Verified CRISPR results identical

prophage_fp = project_dir / 'Prophageblast_Viruses_vs_Hosts.bln'
prophage_header = ['qseqid', 'sseqid', 'pident', 'length', 'qlen', 'slen', 'qstart', 'qend', 'sstart', 'send',
                   'evalue', 'bitscore']

if prophage_fp.exists():

    prophage_df = pd.read_csv(prophage_fp, header=None, names=prophage_header, index_col=None, delimiter='\t')
    prophage_df['bitscore'] = prophage_df['bitscore'].str.replace(',', '')

    prophage_df = prophage_df.drop_duplicates(subset=['qseqid', 'sseqid'], keep='first')  # No order Line 62?

    prophage_df['phage_coverage'] = prophage_df['length'] / prophage_df['qlen']
    prophage_df['host_coverage'] = prophage_df['length'] / prophage_df['slen']

    prophage_df['sseqid'] = prophage_df['sseqid'].apply(lambda x: x.split('_Contig_')[0])

    prophage_df = prophage_df[(prophage_df['pident'] >= 90) & (prophage_df['phage_coverage'] >= 0.3) &
                              (prophage_df['host_coverage'] <= 0.7) & (prophage_df['length'] >= 2500)]

    prophage_subset_df = prophage_df[['qseqid', 'sseqid', 'pident', 'phage_coverage']]

    prophage_subset_df = prophage_subset_df.rename(columns={
        'qseqid': 'Virus', 'sseqid': 'Host', 'pident': 'Percent_ID', 'phage_coverage': 'Viral_coverage'})

    prophage_subset_df = prophage_subset_df.drop_duplicates(subset=['Virus', 'Host'], keep='first')  # TODO Check

    # Scoring
    prophage_subset_df['Prophage_score'] = 1
    for i, df in prophage_subset_df.iterrows():

        viral_coverage = df['Viral_coverage']
        percent_id = df['Percent_ID']

        if viral_coverage >= 0.9 and percent_id >= 98:
            score = 4
        elif viral_coverage >= 0.75:
            score = 3
        elif viral_coverage > 0.5:
            score = 2
        else:
            score = 1  # Minimum, already set

        prophage_subset_df.loc[i, 'Prophage_score'] = score

    prophage_subset_df.index = pd.MultiIndex.from_arrays(prophage_subset_df[['Host', 'Virus']].values.T)

else:
    prophage_subset_df = pd.DataFrame()

# Verified Prophage values

trna_fp = project_dir / 'Viruses_tRNAs_vs_host_tRNAs_Promiscuous_removed.bln'
trna_header = ['qseqid', 'sseqid', 'pident', 'length', 'qlen', 'slen', 'qcovs', 'qstart', 'qend', 'sstart',
               'send', 'evalue', 'bitscore']

if trna_fp.exists():

    trna_df = pd.read_csv(trna_fp, header=None, names=trna_header, index_col=None, delimiter='\t')
    try:
        trna_df['bitscore'] = trna_df['bitscore'].str.replace(',', '')
    except AttributeError:
        pass

    trna_df['qmismatch'] = trna_df['qlen'] - trna_df['length']
    trna_df['smismatch'] = trna_df['slen'] - trna_df['length']

    trna_df = trna_df[(trna_df['pident'] == 100) & (trna_df['qmismatch'] == 0) & (trna_df['smismatch'] <= 2)]
    trna_df = trna_df.sort_values(by=['smismatch'], ascending=True)

    trna_df = trna_df.drop_duplicates(subset=['qseqid', 'sseqid'])

    trna_df['sseqid'] = trna_df['sseqid'].apply(lambda x: x.split('_Contig_')[0])
    trna_df['qseqid'] = trna_df['qseqid'].apply(lambda x: x.split('.trna')[0])

    trna_df = trna_df[(trna_df['pident'] >= 100) & (trna_df['qmismatch'] == 0)]

    trna_subset_df = trna_df[['qseqid', 'sseqid', 'smismatch']]

    trna_subset_df = trna_subset_df.rename(columns={
        'qseqid': 'Virus', 'sseqid': 'Host', 'smismatch': 'Host_terminal_mismatches'})

    trna_subset_df = trna_subset_df.drop_duplicates(subset=['Virus', 'Host'], keep='first')  # TODO Check

    # Scoring
    trna_subset_df['tRNA_score'] = 0
    for i, df in trna_subset_df.iterrows():

        host_mismatches = df['Host_terminal_mismatches']

        if host_mismatches == 0:
            score = 3
        elif host_mismatches == 1:
            score = 2
        elif host_mismatches == 2:
            score = 1
        else:
            score = 0

        trna_subset_df.loc[i, 'tRNA_score'] = score

    trna_subset_df.index = pd.MultiIndex.from_arrays(trna_subset_df[['Host', 'Virus']].values.T)

else:
    trna_subset_df = pd.DataFrame()

# WIsH
wish_fp = project_dir / 'Prediction_pval_lt0.05.list'

if wish_fp.exists():

    wish_header = ['Virus', 'Host', 'LogLikelihood', 'WIsH_pval']
    try:
        wish_df = pd.read_csv(wish_fp, header=0, names=wish_header, index_col=None, delimiter=',')
    except ParserError:
        wish_df = pd.read_csv(wish_fp, header=0, names=wish_header, index_col=None, delimiter='\t')

    # WTH
    wish_df = wish_df[wish_df['Virus'] != 'Phage']  # ???
    wish_df['WIsH_pval'] = pd.to_numeric(wish_df['WIsH_pval'])

    wish_df = wish_df[wish_df['WIsH_pval'] < 0.05]  # LESS THAN ONLY

    wish_df = wish_df.sort_values(by=['WIsH_pval'], ascending=True)

    # Scoring
    # wish_df['WIsH_score'] = 0.5
    for i, df in wish_df.iterrows():
        wish_pval = df['WIsH_pval']

        if wish_pval == 0:
            score = 3
        elif wish_pval < 1e-11:
            score = 2.5
        elif wish_pval < 1e-6:
            score = 2
        elif wish_pval < 1e-3:
            score = 1.5
        elif wish_pval < 1e-2:
            score = 1
        else:
            score = 0.5

        wish_df.loc[i, 'WIsH_score'] = score

    wish_df = wish_df.drop_duplicates(subset=['Virus'], keep='first')

    wish_df.index = pd.MultiIndex.from_arrays(wish_df[['Host', 'Virus']].values.T)

else:
    wish_df = pd.DataFrame()

# Verified WISH scores identical

# Unique virus Line 149, ignoring

# Summarizing all of the data
# I am SHOCKED this block actually works, python = magic

dfs = [crispr_merged_df, prophage_subset_df, trna_subset_df, wish_df]
dfs_to_use = [df for df in dfs if not df.empty]

indices_to_use = [set(df.index.tolist()) for df in dfs_to_use]

# indexes = set(crispr_merged_df.index.tolist()) | set(prophage_subset_df.index.tolist()) | \
#           set(trna_subset_df.index.tolist()) | set(wish_df.index.tolist())
indexes = set().union(*indices_to_use)

columns_to_use = [set(df.columns.tolist()) for df in dfs_to_use]

# columns = set(crispr_merged_df.columns.tolist()) | set(prophage_subset_df.columns.tolist()) | \
#           set(trna_subset_df.columns.tolist()) | set(wish_df.columns.tolist())
columns = set().union(*columns_to_use)

final_df = pd.DataFrame(index=list(indexes), columns=list(columns))

for df_to_use in dfs_to_use:
    final_df.update(df_to_use)

# final_df.update(crispr_merged_df)
# final_df.update(prophage_subset_df)
# final_df.update(trna_subset_df)
# final_df.update(wish_df)

if 'tRNA_score' in final_df.columns.tolist():
    final_df.loc[final_df['tRNA_score'] > 0, 'tRNA_match'] = 'Yes'

scores = ['CRISPR_score', 'Prophage_score', 'tRNA_score', 'WIsH_score']
final_df['Final_score'] = final_df[
    [score for score in scores if score in final_df.columns.tolist()]].sum(axis='columns')

# Rename can handle non-existent
final_df = final_df.rename(columns={
    'Virus': 'Viral population', 'Host': 'Predicted host', 'Number_of_CRISPR_matches': 'Number of CRISPR matches',
    'Max_number_of_mismatches': 'Max number of end mismatches detected in any CRISPR spacer',
    'CRISPR_score': 'CRISPR score', 'Percent_ID': 'Prophage blast percent identity',
    'Viral_coverage': 'Prophage blast viral contig coverage', 'Prophage_score': 'Prophage blast score',
    'tRNA_match': 'tRNA match', 'Host_terminal_mismatches': 'Max number of end mismatches detected in host tRNA',
    'tRNA_score': 'Non-promiscuous tRNA match score', 'WIsH_pval': 'WIsH p-value', 'WIsH_score': 'WIsH score'})

final_columns = ['Viral population', 'Predicted host', 'Final_score', 'Number of CRISPR matches',
                 'Max number of end mismatches detected in any CRISPR spacer', 'CRISPR score',
                 'Prophage blast percent identity', 'Prophage blast viral contig coverage', 'Prophage blast score',
                 'tRNA match', 'Max number of end mismatches detected in host tRNA',
                 'Non-promiscuous tRNA match score', 'WIsH p-value', 'WIsH score']

final_df = final_df[[col for col in final_columns if col in final_df.columns.tolist()]]

final_df.to_csv(project_dir / 'aggregator_table.tsv', index=False, sep='\t')  # Poor MultiIndex...

