VirMatcher
==========

A tool to predict virus-host relationships, leveraging a variety of bioinformatic methods, including; host 
CRISPR-spacers, integrated prophage, host tRNA genes, and k-mer signatures calculated by WIsH. The methodology is 
described in detail in [Gregory et al, 2020](#Citation).

This is a "pythonized" version of aforementioned method, converting most of the bash, perl, R and python into a *nearly* 
single script. Two supporting scripts are included, one integrates the various results from each bioinformatic method (
and R script), while the other is a "workaround" for large scale WIsH runs (python).

**This is _prototype_ code. While every effort has been made to identically preserve the original pipeline, errors may 
 have been made. We are not responsible for any result of its use. While the underlying methodology has been
successfully used, it has not been rigorously compared against other virus-host tools.**

This will be updated *mainly* to streamline the python and implement bug fixes.

## Methodology

The methodology, as mentioned above (and below) are used in [Gregory et al, 2020](#Citation). We provide an 
over-simplified version here, but please do check out the original literature.

Decoy viruses are viruses with known virus-host pairings and are derived from NCBI RefSeq.

Promiscuous tRNAs are derived from (Paez-Espino, et al, Nature 2016). They serve to remove tRNA sequences that would be 
otherwise considered support for a virus-host pair, however, due to their promiscuous nature, cannot be reliably used to 
indicate such a relationship.

## Installation

VirMatcher has several notable dependencies: ([References below](#Supporting-Citations))

 * NCBI BLAST
 * tRNA-Scan SE
 * MINCED
 * WIsH

In addition to several python packages:

 * Biopython >= 1.72
 * Pandas >= 1.1.3 (*Must use >= 1.x*)
 * Psutil >= 5.7.2 (but could get away with lower)

And R...

 * Here
 * Seqinr
 * dplyr
 * Data.table
 * Stringr
 
*Future updates may discontinue R in favor of using the existing python packages. At which point, all R dependencies 
would be dropped. This is already available using the --python-aggregator option.*

The easiest way to install all of VirMatcher's dependencies is through [Miniconda](https://docs.conda.io/en/latest/miniconda.html)

```bash
conda install -c bioconda -c conda-forge minced blast trnascan-se r-here r-seqinr r-dplyr r-data.table r-stringr pandas biopython psutil
git clone https://github.com/soedinglab/WIsH.git && cd WIsH && cmake . && make && chmod +x WIsH && cp WIsH $PATH
git clone https://bitbucket.org/MAVERICLab/virmatcher && cd virmatcher && pip install . --no-deps
```

(As long as WIsH is in your system's $PATH, VirMatcher will find it.)

### Arguments / Inputs

VirMatcher has a number of inputs.

**--virus-fp**: The putative/suspected/known viruses in a single FASTA-formatted file.

**--output-dir**: Output directory. If it doesn't exist, will be created.

**--threads**: Optionally, specify the number of CPU cores to use during processing. VirMatcher will attempt to use the 
maximum, but on some systems it may report the incorrect cores. If that occurs, specify them directly.

**--archaea-host-dir**: (Optional, see below) A folder with potential archaeal host genomes. One genome per file.

**--archaea-taxonomy**: (Optional, see below) A tab-delimited archaeal genome taxonomy file. Should be in the format of "host name \<TAB\> taxon". 
The taxon can be any level.

**--bacteria-host-dir**: (Optional, see below) Same as Archaea, but with Bacteria.

**--bacteria-taxonomy**: (Optional, see below) Same as Archaea, but with Bacteria.

**--decoy-virus-dir** (Optional): Folder with "decoy" viruses. These are viruses with known virus-host pairings.

**--decoy-virus-taxonomy** (Optional): Taxonomy of decoy viruses.

**--python-aggregator**: Switch to a python-based aggregator of the final BLAST, CRISPR, tRNA 
and WIsH results. Performance differences are minimal, and repeated testing has extensively validated identical scoring.

##### For those who want to avoid parsing the inputs...

Getting data into the right format for VirMatcher can be tricky and cause issues if not done correctly. To help with this,
there is now a "--preparer"" option that consumes GTDB-Tk taxonomy outputs and creates the necessary 
VirMatcher input files.

**--preparer**: Enables use of GTDB-Tk processing

**--gtdbtk-out-dir**: The GTDB-Tk output directory, should contain the *.summary.tsv files

**--gtdbtk-in-dir**: The GTDB-Tk input directory. 


*Note: The fasta-formatted files must end in the extension "\*.fasta". This will be fixed in future versions.*

## Processing methodology

Thus, there are 2 main ways to run VirMatcher:

#### (1) Run GTDB-Tk and then VirMatcher

```bash
gtdbtk classify_wf --extension fasta --genome_dir input_dir --out_dir output_dir --pplacer_cpus <cpus>
VirMatcher --preparer --gtdbtk-out-dir output_dir --gtdbtk-in-dir input_dir -v <virus file> --threads <cpus> -o <output directory> --python-aggregator
```

#### (2) Parse the files locally and then run VirMatcher

Optionally, you can select a single domain to predict, simply remove "--archaea-host-dir" and "--archaea-taxonomy" to 
predict bacteria or, remove "--bacteria-host-dir" and "--bacteria-taxonomy" to predict archaea.


```bash
VirMatcher -v <virus file> --archaea-host-dir <archaea directory> --archaea-taxonomy <taxonomy file> --bacteria-host-dir <bacteria directory> --bacteria-taxonomy <taxonomy file> --threads <threads to use> -o <output directory> --python-aggregator
```

## Outputs

A single tab-delimited file, "VirMatcher_Summary_Predictions.tsv" will be produced in the output directory. This file 
has VirMatcher's scoring of each BLAST, CRISPR, tRNA and WIsH result for each virus-host pairing.

### Scores

Interpreting scores can be challenging. The higher the score, the higher the confidence in the virus-host pairing. If 
wanting to follow [Gregory et al, 2020](#Citation), scores of approximately 3 or more were used to assign hosts.

However, even high scores can lead to false positives. The best approach (outside of *in situ* experiments) 
is to select matches with multiple lines of evidence (tRNA, BLAST prophage, CRISPR spacers). The more lines of evidence 
and the more hits within each (e.g., multiple tRNA matches, multiple CRISPR matches) provide more confidence. 
Effectively, this is what the scoring scheme is designed to capture.

## Citation

The theory behind VirMatcher was previously published in:

1. Gregory, A. C. et al. The Gut Virome Database Reveals Age-Dependent Patterns of Virome Diversity in the Human Gut. Cell Host Microbe 28, 724-740.e8 (2020). [DOI: 10.1016/j.chom.2020.08.003](https://doi.org/10.1016/j.chom.2020.08.003)

If you'd like to cite this repository for the python "version," feel free to do so.

B. Bolduc and A. Zayed, py-VirMatcher, (2020), BitBucket repository, https://bitbucket.org/MAVERICLab/virmatcher/

### Supporting Citations

These tools help make VirMatcher possible:

1. Minced: Mining CRISPRs in Environmental Datasets [https://github.com/ctSkennerton/minced/tree/master]

2. Lowe, T. M. & Eddy, S. R. tRNAscan-SE: A Program for Improved Detection of Transfer RNA Genes in Genomic Sequence. Nucleic Acids Res. 25, 0955–0964 (1997). [https://doi.org/10.1093/nar/25.5.955](https://academic.oup.com/nar/article-abstract/25/5/955/5133591)

3. Galiez, C., Siebert, M., Enault, F., Vincent, J. & Söding, J. WIsH: Who is the host? Predicting prokaryotic hosts from metagenomic phage contigs. Bioinformatics 1–2 (2017). [https://doi.org/10.1093/bioinformatics/btx383](https://academic.oup.com/bioinformatics/article/33/19/3113/3964377)

4. Altschul, S. F., Gish, W., Miller, W., Myers, E. W. & Lipman, D. J. Basic local alignment search tool. J. Mol. Biol. 215, 403–410 (1990). [https://doi.org/10.1016/S0022-2836(05)80360-2](https://www.sciencedirect.com/science/article/pii/S0022283605803602)

5. Paez-Espino, D. et al. Uncovering Earth’s virome. Nature 536, 425–430 (2016). [https://doi.org/10.1038/nature19094](https://www.nature.com/articles/nature19094)

**Please cite these!**

## Future Work

There's a list of updates forthcoming, and will be worked on at a glacial pace.

 * Continue filling out methodology with additional information
 
 * Expand description of inputs
 
 * Highlight tool dependencies used
 
 * Detail promiscuous tRNAs used
 
 * Continue adding checkpoints to prevent re-running large sections of the code
